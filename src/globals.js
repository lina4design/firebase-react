import React from "react";
import {
  About,
  Contacts,
  Home,
  Post,
  AdminAbout,
  AdminContacts,
  AdminHome
} from "./components";

export { default as LOGO } from "./images/logo.png";
export const GOOGLE_API_KEY = "AIzaSyBBYO3snN1YHWANBjVi2IwazhOWRdbC5Jo";

export const NAV_MAP = [
  {
    path: "/",
    title: "Home",
    page: () => <Home />
  },
  {
    path: "/about",
    title: "About",
    page: () => <About />
  },
  {
    path: "/contacts",
    title: "Contacts",
    page: () => <Contacts />
  },
  {
    hidden: true,
    path: "/about/post",
    title: "Post",
    page: () => <Post />
  },
  {
    hidden: true,
    path: "/admin",
    title: "Admin",
    page: () => <AdminHome />
  },
  {
    hidden: true,
    path: "/admin/about",
    title: "Admin",
    page: () => <AdminAbout />
  },
  {
    hidden: true,
    path: "/admin/contacts",
    title: "Admin",
    page: () => <AdminContacts />
  }
];
export const CONFIG = {
  apiKey: "AIzaSyBbjDRp9Fivw3g6UktnSF0ooaAcq5lAbQw",
  authDomain: "react-app-1-c80ca.firebaseapp.com",
  databaseURL: "https://react-app-1-c80ca.firebaseio.com",
  projectId: "react-app-1-c80ca",
  storageBucket: "react-app-1-c80ca.appspot.com",
  messagingSenderId: "554976150303"
};
