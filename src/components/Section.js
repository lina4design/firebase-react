import React from "react";
export class Section extends React.Component {
  render() {
    const { color } = this.props;
    return (
      <div className={`-container ${color}-- p-5`}>{this.props.children}</div>
    );
  }
}
