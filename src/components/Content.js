import React from "react";
export class Content extends React.Component {
  render() {
    const { importance: imp, header, text, position } = this.props;
    const HeaderTag = `h${imp}`;
    switch (position) {
      case "left":
        return (
          <div className="row no-gutters">
            <div className="col content">
              <HeaderTag>{header}</HeaderTag>
              <p>{text}</p>
            </div>
            {this.props.children}
          </div>
        );
      case "right":
        return (
          <div className="row">
            {this.props.children}
            <div className="col content">
              <HeaderTag>{header}</HeaderTag>
              <p>{text}</p>
            </div>
          </div>
        );
      case "center":
        return (
          <div className="row">
            <div className="col-10 flex-col-center -container">
              <HeaderTag>{header}</HeaderTag>
              <p>{text}</p>
            </div>
            {this.props.children}
          </div>
        );
      default:
        return <div className="row no-gutters">{this.props.children}</div>;
    }
  }
}
