import React from "react";
import { Header, Section, Content, Footer, Post } from "../components";
import { withRouter } from "react-router-dom";
class Home extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    console.log("paspaudei diva");
    this.props.history.push("/about/post");
  }
  renderSquare() {
    return;
  }
  renderLine() {
    return (
      <div className="row">
        {this.renderSquare()}
        {this.renderSquare()}
        {this.renderSquare()}
        {this.renderSquare()}
      </div>
    );
  }
  renderPortfolio() {
    return (
      <div>
        {this.renderLine()}
        {this.renderLine()}
        {this.renderLine()}
      </div>
    );
  }
  render() {
    return (
      <div>
        <Header />
        <Section color="light">
          <Content
            importance={2}
            header="UI/UX and Graphic Designer"
            text="Change the visual order of specific flex items with a handful of order utilities. We only provide options for making an item first or last, as well as a reset to use the DOM order."
            position="center"
          />
        </Section>
        <div className="row">
          {new Array(3 * 4).fill().map((_, i) => (
            <div
              style={{
                backgroundImage: `url("https://picsum.photos/300?v=${i + 1}")`
              }}
              className="image-square col-12 col-sm-6 col-lg-4 col-xl-3"
              onClick={this.handleClick}
            />
          ))}
        </div>
        {this.renderPortfolio()}
        <Footer />
      </div>
    );
  }
}
export default withRouter(Home);
