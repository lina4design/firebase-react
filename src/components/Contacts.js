import React from "react";
import {
  Header,
  ImageSection,
  Content,
  ContactForm,
  Map,
  Footer
} from "../components";
export class Contacts extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <ImageSection color="light">
          <Content
            importance={2}
            position="left"
            header="Contact us"
            text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          >
            <div className="col-7">
              <ContactForm>
                <button type="submit" className="btn">
                  Contact
                </button>
              </ContactForm>
            </div>
            <Map ratio={1 / 3} />
          </Content>
        </ImageSection>
        <Footer />
      </div>
    );
  }
}
