import React from "react";
import firebase from "../database";
export class AdminHome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: {
        HomePage: {
          section1: {
            header: "",
            content: ""
          }
        }
      }
    };
  }
  componentDidMount() {
    const sectionRef = firebase
      .database()
      .ref("HomePage")
      .orderByKey()
      .limitToLast(100);

    sectionRef.on("value", snap => {
      this.setState({
        ...this.state,
        loading: false,
        data: { ...this.state.data, HomePage: snap.val() }
      });
    });
  }
  onSubmit() {
    const newContent = this.state.data.HomePage;

    firebase
      .database()
      .ref("HomePage")
      .set(newContent);
  }
  render() {
    const {
      loading,
      HomePage: { section1 }
    } = this.state;

    debugger;

    return (
      <div>
        {loading ? (
          "Kraunasi"
        ) : (
          <form>
            <h2>Home lango turinys</h2>
            <h3>1 skyrius</h3>
            <h4>Antraštė</h4>
            <input
              type="text"
              name="header"
              ref={el => (this.inputForHeader = el)}
              value={section1.header}
            />
            <h4>Turinys</h4>
            <input
              type="text"
              name="content"
              ref={el => (this.inputForContent = el)}
              value={section1.content}
            />

            <button
              className="btn"
              type="submit"
              onClick={this.onSubmit.bind(this)}
            >
              Patvirtinti
            </button>
          </form>
        )}
      </div>
    );
  }
}
