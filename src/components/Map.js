import React from "react";
import { GOOGLE_API_KEY } from "../globals";
export const Map = ({ ratio = 16 / 9 }) => (
  <div
    className="Map"
    style={{ paddingBottom: (100 / (1 / ratio)).toString() + "%" }}
  >
    <iframe
      width="600"
      height="450"
      frameborder="0"
      src={`https://www.google.com/maps/embed/v1/place?key=${GOOGLE_API_KEY}
  &q=Space+Needle,Seattle+WA`}
      allowfullscreen
    />
  </div>
);
