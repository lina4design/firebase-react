import React from "react";
export class ImageSection extends React.Component {
  render() {
    const { color } = this.props;
    return <div className={`${color}--`}>{this.props.children}</div>;
  }
}
