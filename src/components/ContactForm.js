import React from "react";
import firebase from "../database";
export class ContactForm extends React.Component {
  constructor() {
    super();
    this.state = {
      submitedForms: {
        name: "",
        mail: "",
        title: "",
        message: ""
      }
    };
  }

  onSubmit(e) {
    e.preventDefault();
    // <- prevent form submit from reloading the page
    /* Send the message to Firebase */
    const formData = {
      name: this.inputForName.value,
      title: this.inputForTitle.value,
      mail: this.inputForMail.value,
      message: this.inputForMessage.value
    };
    firebase
      .database()
      .ref("submitedForms")
      .push(formData)
      .then(() => {})
      .catch(() => {
        console.log("neveikia :(");
      });
  }
  render() {
    return (
      <form onSubmit={this.onSubmit.bind(this)} className="p-4">
        <div className="row no-gutters">
          <div className="col m-1 ml-0">
            <input
              className=""
              name="name"
              type="text"
              ref={el => (this.inputForName = el)}
              placeholder="Name"
            />
          </div>
          <div className="col m-1 ml-0">
            <input
              className=""
              name="mail"
              type="text"
              ref={el => (this.inputForMail = el)}
              placeholder="Email"
            />
          </div>
        </div>
        <div className="row no-gutters">
          <div className="col m-1 ml-0">
            <input
              type="text"
              name="title"
              ref={el => (this.inputForTitle = el)}
              placeholder="Title"
            />
          </div>
        </div>
        <div className="m-1 ml-0">
          <textarea
            className=""
            placeholder="Message"
            name="message"
            ref={el => (this.inputForMessage = el)}
          />
        </div>
        {this.props.children}
      </form>
    );
  }
}
