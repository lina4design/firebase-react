import React from "react";
import {
  Header,
  Section,
  Content,
  IconBlock,
  ImageSection,
  Footer
} from "../components";
import { NavLink } from "react-router-dom";

export class About extends React.Component {
  handleClick() {}
  render() {
    const blocks = [
      { icon: "portfolio", number: 265, text: "Some text" },
      { icon: "clock", number: 546, text: "More text" },
      { icon: "star", number: 135, text: "Even more text" },
      { icon: "like", number: 134, text: "text..." }
    ];

    return (
      <div>
        <Header />
        <Section color="dark">
          <Content
            importance={1}
            header="Welcome!"
            text="Nice to see you here!"
            position="center"
          />
        </Section>
        <Section color="color">
          <Content>
            {blocks.map(b => <IconBlock {...b} key={b.icon} />)}
          </Content>
        </Section>
        <ImageSection color="light">
          <Content
            importance={2}
            header="About me"
            text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            position="right"
          >
            <div className="image col" />
          </Content>
        </ImageSection>
        <Section>
          <Content
            importance={2}
            header="Have any questions?"
            text="Contact us!"
            position="center"
          />
          <NavLink to="/contacts" exact key="Contacts">
            <button className="btn">Contact</button>
          </NavLink>
        </Section>
        <Footer />
      </div>
    );
  }
}
