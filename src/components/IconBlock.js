import React from "react";
export const IconBlock = ({ icon, number, text, ...otherProps }) => (
  <div className={"col p-0"} {...otherProps}>
    <i className={`icon pe-lg pe-3x pe-7s-${icon}`} />
    <p className={"icon-text"}>
      <strong>{number}</strong>
      <br />
      {text}
    </p>
  </div>
);
