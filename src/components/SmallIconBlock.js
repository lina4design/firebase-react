import React from "react";

export const SmallIconBlock = ({ icon, text, position }) => {
  if (position === "left") {
    return (
      <div className="col p-0 d-flex align-items-center">
        <i className={`p-1 pe-lg pe-7s-${icon}`} />
        <p className="display-ib m-0 p-2">{text}</p>
      </div>
    );
  } else
    return (
      <div className={"col p-0 d-flex align-items-center"}>
        <p className="display-ib m-0 p-2">{text}</p>
        <i className={`p-1 pe-lg pe-7s-${icon}`} />
      </div>
    );
};
