import React from "react";
import { LOGO, NAV_MAP } from "../globals";
import { NavLink } from "react-router-dom";
export class Header extends React.Component {
  render() {
    return (
      <nav className="d-flex align-items-center ">
        <img src={LOGO} alt="" className="logo" />
        <ul className="navigation">
          {NAV_MAP.map(
            ({ path, title, hidden }) =>
              hidden ? null : (
                <li>
                  <NavLink to={path} exact key={title}>
                    {title}
                  </NavLink>
                </li>
              )
          )}
        </ul>
      </nav>
    );
  }
}
