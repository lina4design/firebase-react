import React from "react";
import {
  Header,
  ImageSection,
  Content,
  SmallIconBlock,
  Footer
} from "../components";
export class Post extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <ImageSection color="light">
          <Content
            importance={1}
            header="Some Title"
            text="We may think that Board should just
        ask each Square for the Square’s state. Although this approach
        is possible in React, we discourage it because the code becomes
        difficult to understand, susceptible to bugs, and hard to refactor.
        Instead, the best approach is to store the game’s state in the parent
        mponent instead of in each Square. The Board component can tell
        each Square what to display by passing a prop, just like we did
        when we passed a number to each Square."
            position="left"
          >
            <div className="image col" />
          </Content>
        </ImageSection>
        <div className="d-flex justify-content-between align-items-center p-1">
          <div>
            <SmallIconBlock icon="angle-left" text="Previous" position="left" />
          </div>
          <i className="pe-lg pe-7s-keypad" />
          <div>
            <SmallIconBlock icon="angle-right" text="Next" position="right" />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
