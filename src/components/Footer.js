import React from "react";
export class Footer extends React.Component {
  render() {
    return (
      <footer>
        <div className="row footer-container no-gutters">
          <div className="col">
            <h5>PHXdesign, Inc.</h5>
            <small> &copy; 2018 PHXdesign. All rights reserved.</small>
          </div>
          <div className="col">
            <ul>
              <li>
                <small>hello@phxdesign.com</small>
              </li>
              <li>
                <small>+44 987 065 908</small>
              </li>
            </ul>
          </div>
          <div className="col">
            <ul>
              <li>
                <small>Projects</small>
              </li>
              <li>
                <small>About</small>
              </li>
              <li>
                <small>Services</small>
              </li>
              <li>
                <small>Carrer</small>
              </li>
            </ul>
          </div>
          <div className="col">
            <ul>
              <li>
                <small>Facebook</small>
              </li>
              <li>
                <small>Twitter</small>
              </li>
              <li>
                <small>Instagram</small>
              </li>
              <li>
                <small>Dribbble</small>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    );
  }
}
